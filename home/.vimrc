set nocompatible
syntax on
filetype off


filetype plugin indent on
"map! <C-a> <Home>
"map! <C-e> <End>
"map! <C-d> <Delete>
"map! <C-f> <Right>
"map! <C-b> <Left>
"map! <C-k> <Esc>lv<End>hxa
set noerrorbells
set vb t_vb=


set autoindent
set shiftwidth=4
set ts=4
set expandtab
set number
set hlsearch
set scrolloff=10


" for US配列
noremap ; :
noremap : ;
""

nnoremap <C-a> ^
nnoremap <C-e> $

nnoremap <C-c> <Esc>^i//<Esc>

