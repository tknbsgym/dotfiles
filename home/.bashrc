# .bashrc

[ -z "$PS1" ] && return


# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


# User specific aliases and functions
export CLICOLOR=1
export LSCOLORS=gxfxcxdxbxegedabagacad

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias la='ls -lah'
alias a='ls -lah'
alias ud='cd .. && pwd'

export PATH="$HOME/.anyenv/bin:$PATH"
eval "$(anyenv init -)"
