Setup

```
#!sh


# リポジトリをクローン
cd ~ && git clone https://tknbsgym@bitbucket.org/tknbsgym/dotfiles.git

# シンボリックリンクを作成
ln -s ~/dotfiles/home/.vimrc ~/.vimrc
ln -s ~/dotfiles/home/.bash_profile ~/.bash_profile
ln -s ~/dotfiles/home/.bashrc ~/.bashrc
ln -s ~/dotfiles/home/.gitconfig ~/.gitconfig
ln -s ~/dotfiles/home/.tmux.conf ~/.tmux.conf

```


## すでに ~/.vimrc が存在する場合

~/.vimrc を開いて以下の一行を追記

```
#!sh
source ~/dotfiles/home/.vimrc
```

